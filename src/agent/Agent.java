package agent;

import gng.*;
import temporality.Temporality;

/**
 * Interface des Agents Geamas-NG.
 * <br><br>
 * Tous les agents doivent implémenter cette interface pour être reconnu par la plateforme Geamas-NG.
 * <br><br>
 * <font color=red>ATTENTION :</font> la classe définissant l'agent doit avoir un constructeur qui prend un <a href="..\type\ReadableMultiChamp.html">ReadableMultiChamp</a> comme argument !
 * Cet argument contient les paramêtres d'initialisation fournit dans la demande d'instanciation de l'agent.
 * <br><br>
 * <b>Voir :</b> <a href="../../../tutorial/agent.html">Programmer un agent GNG</a>
 */
public interface Agent {

 /**
  * Notification de l'enregistrement de l'agent auprés d'un noyau d'exécution.
  * <br> 
  * Cette méthode invoqué au moment ou l'agent est enreigistré auprés d'un noyau d'exécution.
  * 
  * @param state la struture d'état de l'agent pour le noyau sur lequelle il est enregistré 
  */	
 public void notifyRegister(State state);
 
 /**
  * Notification du désenregistrement de l'agent auprés d'un noyau d'exécution.
  * <br> 
  * Cette méthode invoqué au moment ou l'agent est désenregistré auprés d'un noyau d'exécution.
  * 
  * @param state la struture d'état de l'agent auprés noyau qui le désenregistre
  */	 
 public void notifyUnregister(State state);

 /**
  * Demande d'initialisation des Temporalités.
  * <br>
  * Cette méthode est invoqué par le noyau d'exécution juste avant le lancement de la simulation. 
  * A travers cette méthode l'agent est invité à définir ses Temporalités.
  * <br><br>
  * <b>NB:</b> si aucune temporalité n'est défini dans cette méthode, le système affectera automatiquement à l'agent une temporalité par défaut dont la validité couvrira toute la durée de la simulation et la péridode sera celle défini par défaut par l'expérimentateur.
  * 
  * @param s structure d'état de l'agent pour le noyau d'exécution appelant.
  * @param t 
  */
 public void initTemporality(State s, Temporality t);
 
 /**
  * Exécution du code du comportement de l'agent.
  * <br>
  * Au cours de la simulation, cette méthode sera invoqué à chaque validation d'un temporalité de l'agent.
  *   
  * @param state	structure d'état de l'agent pour le noyau d'exécution appelant.
  * @param t		temporalité qui a été validé et qui a provoqué l'appelle de cette méthode.
  * @param mb		boite au lettre de l'agent dans le noyau d'exécution appelant.
  * @param body		corps via lequel l'agent peut intéragir avec l'environnement. 
  */
 public void run(State state, Temporality t, Mailbox mb, Body body);
 
}