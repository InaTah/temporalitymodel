package agent;

import gng.*;
import temporality.InternalTemporalRule;
import temporality.InternalTemporality;
import temporality.Timer;
import temporality.Temporality;

public class AgentDescriptor {

 //Attribut
 Timer timer;	
 String id;
 Agent agent;
 ReadableMultiChamp bodyParam;
 Body body;
// InternalState state;
 State state;
 InternalTemporality temporality;
// InternalMailbox mailbox;

 //M�thode
 public AgentDescriptor(Kernel kernel,String family,String v_id,Agent a,ReadableMultiChamp bp,Timer t){
	 timer=t;
	 id=v_id;
	 agent=a;
//	 state=new InternalState(kernel,v_id,family);
	 temporality=new InternalTemporality(this,timer);
//	 mailbox=new InternalMailbox(this);
	 bodyParam=bp;
	 body=null;
 }

 public void notifyRegister(){ 
	// agent.notifyRegister(state); 
	 }
 public void notifyUnregister(){
	// agent.notifyUnregister(state);
	 }
 
 public State getState(){  return(state); }
 
 /**
  * Initialisation de l'agant
  */
 protected void initialise(Environment env){
	 temporality.reset();	//on vide les rêgles temporelle précédemment défini
	 
//	 body=env.newBody(bodyParam);
	 agent.initTemporality(state,temporality);
	 
	 //si l'agent n'a pas défini de règle temporelle, on lui assigne la rêgle par défaut
	 if(temporality.countRule()==0) temporality.newRule("#default",Temporality.DEFAULT);
 }
 
 /**
  * Envoie de message
  */
 public void sendMessage(String dest,Object content,long delay){	//envoie un message
	 timer.createMessage(id,dest,content,delay);
 }
 
 /**
  * Enregistrement / desenregistrement des r�gle de l'agent
  */
 public boolean register(InternalTemporalRule tr){ return(timer.register(tr)); }
 public void unregister(InternalTemporalRule tr){ timer.unregister(tr); }
 
 /**
  * Execution du comportement de l'agent
  */
 public void run(){
//	 agent.run(state,temporality,mailbox,body);
 }
 
 /**
  * R�ception d'un message
  */
 public void receive(InternalMessage mess){
//	 mailbox.receive(mess);
 }
}