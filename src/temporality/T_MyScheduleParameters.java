/**
 * Inspired from ScheduleParameters.java in Repast and InternalTemporalRule.java in Temporality model
 */
package temporality;
/**
 * Parameters describing when an action should be scheduled. Use the static
 * <code>create*</code> methods to create the appropriate ScheduleParameter
 * object.
 * 
 * @see repast.simphony.engine.schedule.ISchedule
 * 
 * @author Nick Collier
 * @version $Revision: 1.1 $ $Date: 2005/12/21 22:25:34 $
 */

public class T_MyScheduleParameters {
	/**
	 * Constant representing no duration.
	 */
	public static final double NO_DURATION = -1;
	/**
	 * Constant representing end of simulation as the tick at which to execute the
	 * actions.
	 */
	public static final double END = Double.POSITIVE_INFINITY;

	/**
	 * Constant representing random priority.
	 */
	public static final double RANDOM_PRIORITY = Double.NaN;
	public static final double FIRST_PRIORITY = Double.POSITIVE_INFINITY;
	public static final double LAST_PRIORITY = Double.NEGATIVE_INFINITY;

	//String id;
	private double start,end;//Begin and end of an action
	private double gap; //Period and variability
	private double period;
	private int hashCode = 17;

	//Equivalent to define() method in InternalTemporalRule
	protected T_MyScheduleParameters(double start, double end, double period, double gap){
		this.start = start;
		this.end = end;
		this.period = period;
		//this.interval = interval;
		this.gap = gap;
		//this.priority = priority;
		//this.duration = end-start;

	}

	public double getStart() {
		return start;
	}

	public double getEnd() {
		return end;
	}
	
	public void setEnd(double endTime){
		end= endTime;
	}

	public double getGap() {
		return gap;
	}

	public double getPeriod() {
		return period;
	}
	


	/**
	 * Gets the hash code for this schedule parameters.
	 * 
	 * @return this schedule parameters hash code
	 */
	@Override
	public int hashCode() {
		return hashCode;
	}

	/**
	 * Checks if this ScheduleParamters equals the passed in object. The passed
	 * object is equal to this one if all its parameters (start, end, etc.)
	 * are equal to this one.
	 * 
	 * @return if the object is equal to the current one as defined above
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof T_MyScheduleParameters)) {
			return false;
		}
		T_MyScheduleParameters otherParams = (T_MyScheduleParameters) obj;

		return otherParams.start == this.start 
				&& otherParams.end == this.end
				&& otherParams.period == this.period
				&& otherParams.gap == this.gap;
	}

	public String toString() {
		return String
				.format(
						"ScheduleParameters[start: %f, end: %f, frequency: %f, gap: %f]",
						start, end, period, gap);
	}




}
