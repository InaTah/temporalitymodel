package temporality;

/**
 * Décrit une régle temporelle
 * <br><br>
 * <b>Voir :</b> <a href="../../../tutorial/agent.html">Programmer un agent GNG</a>
 */

/**
 * Lecture de l'identifiant de la règle
 * @return identifiant de la règle
 */
public interface T_IMyTemporalRule {


	public String getId();		//identifiant de la règle
	/**
	 * Lecture de la période de la règle
	 * @return période de la règle
	 */

	public double getBeginTime();	//date de début de valididé de la règle


	/**
	 * Retourne le nombre de fois que la règle à conduit a l'activation de l'agent
	 * @return nombre d'activation produite par cette règle
	 */
	public int cycleCount();		//nombre de fois utilisé
	/**
	 * Lecture de la date de prochaine activation que produira cette règle.
	 * @return date de prochaine activation, -1 si cette date est encore indéterminée
	 */
	public double getNextTime();	//date de prochain déclenchement (-1 si indéterminé pour l'instant)

	/**
	 * Spécifie si la prochaine activation produite par cette règle doit être annulé ou non.
	 * Une annulation n'est possible que si la prochaine date d'activation est connu
	 * @param flag true si la prochaine activation doit être annulé
	 */
	public void cancelNext(boolean flag); // si flag==true annul la prochaine activation de cette règle (cette opération n'est possible que si la prochaine date d'activation est connu)

	/**
	 * Test si la prochaine activation produite par cette règle est annulée
	 * @return true si la prochaine activation de la règle est annulée
	 */
	public boolean isCanceled();	//test si l'activation de cette règle au cours de ce cycle a été explicitement annulé (via Temporality.cancelRule(...))

	/**
	 * Test si la régle a déja été activé au cours de ce cycle
	 * @return true si la règle a déjà été activé au cours de ce cycle
	 */
	public boolean isActivated(); //test si l'activation de cette règle a déjà donné lieu a un appel a la méthode run de l'agent au cours de ce cycle



}
