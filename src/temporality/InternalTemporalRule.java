package temporality;

public class InternalTemporalRule implements TemporalRule {

 //Attribut
 InternalTemporality temporality;	
 String id;		//identifiant
 long beginTime,endTime;
 long periode,gap;
 long timeCanceled,timeActivated;

 int compteur;
 TimeSlot timeSlot;	//prochain timeSlot d'exécution (ou time slot courant si la rêgle est activé)
 long nextTime;		//prochaine d'ate d'exécution (-1 si indéterminé), NB: si la rêgle n'est pas activé on a nextTime==timeSlot.getTime()
 
 //Constructeur
 public InternalTemporalRule(InternalTemporality tempo,String ruleId,long beginT,long endT,long p,long g){
	 timeActivated=-2;
	 timeCanceled=-2;
	 temporality=tempo;
	 id=ruleId;
	 define(beginT,endT,p,g);
 }
 
 public void define(long beginT,long endT,long p,long g){
	 beginTime=beginT; endTime=endT;
	 periode=p; gap=g;
	 compteur=0;
	 timeCanceled=-1;
	 nextTime=-1;
 }
 
 //[Interface TemporalRule
 public String getId(){ return(id); }		//identifiant de la r�gle
 public long getPeriod(){ return(periode); }//période de la règle (-1 si règle ponctuel)  
 public long getGap(){ return(gap);}		//écart toléré entre date exigé et date effective
 public long getBeginTime(){ return(beginTime);} //date de début de valididé de la règle
 public long getEndTime(){ return(endTime); }  //date de fin de valididé de la règle

 public int cycleCount() {return(compteur); }  //nombre de fois que la rèele a été déclenché
 
 public long getNextTime(){	//date de prochain déclenchement de cette rêgle (valable ssi periode!=ALL_CYCLE , renvoie -1 dans ce cas)
	 return(nextTime);
 } 
 public void cancelNext(boolean flag){ // si flag==true annul la prochaine activation de cette règle (cette opération n'est possible que si cette régle n'est ou n'a pas déja été activé au cours du cycle courant et que la prochaine date d'activation est connu)
	 if(flag) { if(!isActivated()) timeCanceled=timeSlot.getTime(); }
	 else timeCanceled=-2;
 }
 public boolean isCanceled(){ return(timeCanceled==timeSlot.getTime());}
 public boolean isActivated(){ return(timeActivated==temporality.getTime()); } //test si l'activation de cette règle a déjà donné lieu a un appel a la méthode run de l'agent au cours de ce cycle 
 //]

 //Méthode
 public long getSlotTime(){	//lecture de la date du TimeSlot affect� a cette r�gle
	 if(timeSlot!=null) return(timeSlot.getTime());
	 return(-1);
 }
 
 public void setTimeSlot(TimeSlot ts){
	 timeSlot=ts;
	 nextTime=-1;	//indéterminé
	 if(periode==Temporality.PONCTUAL) endTime=ts.getTime(); //rêgle ponctuelle
 }
 
 public TimeSlot getTimeSlot() { return(timeSlot); }
 
 public void setNextTime(long nt) { nextTime=nt; }
 
 public boolean isCanceled(long t){ return(timeCanceled==t);}
 
 /**
  * Active cette rêgle temporelle (=> execute le comporte associé)
  */
 public void run(){
	 compteur++;
	 timeActivated=temporality.getTime();
	 temporality.run(this);
 }
 
}