package temporality;

import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.environment.RunEnvironmentBuilder;
import repast.simphony.engine.environment.Runner;
import repast.simphony.engine.schedule.IScheduleFactory;
import repast.simphony.parameter.Parameters;
import repast.simphony.parameter.EmptyParameters;

public class T_MyRunEnvironment implements RunEnvironmentBuilder {
	private IScheduleFactory scheduleFactory = new T_MyScheduleFactory();
	private Runner runner;
	private boolean isBatch;
	private Parameters parameters = new EmptyParameters();

	@Override
	public Runner getScheduleRunner() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IScheduleFactory getScheduleFactory() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setScheduleFactory(IScheduleFactory scheduleFactory) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Parameters getParameters() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setParameters(Parameters parameters) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public RunEnvironment createRunEnvironment() {
		RunEnvironment.init(scheduleFactory.createSchedule(), runner, parameters, isBatch);
		return RunEnvironment.getInstance();
		// TODO Auto-generated method stub
		//return null;
	}

}
