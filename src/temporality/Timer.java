package temporality;

import java.util.*;
import gng.*;

/**
 * Classe qui gère l'horloge virtuelle (timecode début/fin, passage au prochain timecode, etc...)
 * @author Denis
 *
 */
public class Timer {
  
 //horloge virtuelle		
 long startSimulation,endSimulation;//date du début et de fin de simulation
 long defaultStep,minimalStep;		//pas par défaut, pas mimnimun
		
 //Interaction avec la plateforme de simualtion
 Kernel kernel;				//noyau d'exécution (pour livrer les messages)
 Environment environment;	//l'environement
 LinkedList<InternalMessage> waitingMessage;	//pile des messages en attente (tri� par ordre croissant des dates de livraison)

 //Elément interne (pour l'implémentation) pour la gestion du temps virtuel
 TimeSlot nextTimeSlot;	//prochain slot temporel a exécuter	
 TimeSlot forAllCycle;	//timeslot a dêclencher a tous les cycles	
 
  //M�thode
 public Timer(Kernel k){
	 kernel=k;
	 environment=null;
	 startSimulation=-1; endSimulation=-1; defaultStep=-1;
	 nextTimeSlot=null; forAllCycle=null;
	 waitingMessage=new LinkedList<InternalMessage>();
 }
 
 /**
  * Initialisation du Timer
  */
 public void initialise(Environment env,long start,long end,long step,long mini){
	 environment=env;
	 startSimulation=start; endSimulation=end; 
	 defaultStep=step; minimalStep=mini;
	 purge();	//on purge l'ensemble des TimeSlot existant
 }
 
 public long getMinimalStep(){ return(minimalStep); }
 public long timeStartSimulation(){ return(startSimulation);}
 public long timeEndSimulation(){ return(endSimulation);}
 
 /**
  * Test si l'ex�cution est termin� (ou non initialis�)
  */
 public boolean terminated(){ return(nextTimeSlot==null || nextTimeSlot.getTime()>endSimulation);}

 /**
  * Enregistre une nouvelle règle temporelle
  */ 
 public boolean register(InternalTemporalRule tr){
	 if(startSimulation==-1) return(false); //Timer non initialisé

	 unregister(tr); //on désenregistrer la règle (au cas ou elle serait déja enregistré)
	 
	 long periode=tr.getPeriod(); 
	 long time=tr.getBeginTime(); long endT=tr.getEndTime();

	 long next_time=nextTimeSlot.getTime();
	 
	 if(endT<next_time) return(true); //règle périmé
		 
	 if(periode>=0 && periode<minimalStep) periode=minimalStep; //pas minimum
	 
	 if(periode==Temporality.ALL_CYCLE){ 
		 forAllCycle.add(tr);
		 return(true);
	 } else if(periode==Temporality.DEFAULT) time=nextStep(time,next_time,defaultStep);
	 else if(periode>0) time=nextStep(time,next_time,periode);
	
	 if(endT<time)  return(true); //règle périmé
	 
	 nextTimeSlot.place(tr,time,periode);
//on affiche le trace timeSlot
//System.out.println("[ TRACE SLOT -----------------------");
//nextTimeSlot.trace();
//System.out.println("----------------------- TRACE SLOT ]");
	 return(true);
 }
 
 /**
  * Cr�ation d'un nouveau message
  */
 public void createMessage(String exp,String dest,Object content,long delay){
	 if(nextTimeSlot!=null){
		 long dateE=nextTimeSlot.getTime();
		 long dateR;
		 if(delay<=0) dateR=0;
		 else dateR=dateE+delay;
		 InternalMessage mess=new InternalMessage(exp,dest,content,dateE,delay,dateR);
		 add(mess,dateR);
	 }
 }
 
 /**
  * Desenregistre une r�gle temporelle
  */
 public void unregister(InternalTemporalRule tr){
	 TimeSlot ts=tr.getTimeSlot();
	 if(ts!=null) ts.remove(tr);
	 tr.setTimeSlot(null);
 }
 
 /**
  * Calcul la position du TimeSlot le plus pr�s situ� apr�s 'pos'
  * @param ref
  * @param pos
  * @param periode
  * @return
  */
 public long nextSlot(long pos){
	 if(pos<=startSimulation) return(startSimulation);
	 if(minimalStep==0) return(pos);
	 float dec=(((float) (pos-startSimulation))/((float) minimalStep)) + 0.5f;
	 return((long) (startSimulation+ (dec * minimalStep)));
 }
 
 /**
  * Exécution du prochain time slot
  *
  */
 protected boolean runNextSlot(){
	 if(!terminated()){
		 long currentTime=nextTimeSlot.getTime();
		 kernel.fireRunCycle(currentTime,startSimulation,endSimulation);	   //on signale le d�but d'ex�cution du cycle
		 
		 //On execute l'environnement
		 if(environment!=null) environment.run(currentTime);
		 
		 deliverMessages(currentTime); //On livre les messages qui doivent l'�tre au cours de ce cycle

		 forAllCycle.runAsAllCycle(); //on exécute les comportement qui doivent être exécuté à tous les cycles		 
		 long next_time=nextTimeSlot.run();		 //on exécute le prochain slot temporel
	 
		 TimeSlot newNextSlot=nextTimeSlot.getNextSlot();
		 
		 //on repositionne le slot
		 if(next_time<=endSimulation && nextTimeSlot.ruleCount()>0){ 
			 nextTimeSlot.setTime(next_time);
			 
			 if(newNextSlot==null) newNextSlot=nextTimeSlot;
			 else if(next_time<newNextSlot.getTime()) newNextSlot=nextTimeSlot; //on change pas la chaine temporelle
			 else {
				 nextTimeSlot.setNextSlot(null);
				 newNextSlot.place(nextTimeSlot,next_time);
			 }
	 	 }
		 
		 nextTimeSlot=newNextSlot;
		 
		 //on teste si la simulation est termin�
		 if(nextTimeSlot==null || nextTimeSlot.getTime()>endSimulation){
			 forAllCycle.setTime(-1);
			 kernel.fireEndSimulation();
			 return(false);
		 } else{
			 forAllCycle.setTime(nextTimeSlot.getTime());
			 return(true);
		 }
	 } else return(false);
 }

 public long nextStepFor(TimeSlot ts){
	 long actual=ts.getTime(); long step=ts.getPeriod();
	 long nextTime;
	 if(step==Temporality.DEFAULT) nextTime=nextSlot(actual+defaultStep);
	 else if(step>0) nextTime=nextSlot(actual+step);
	 else nextTime=-1;	//Temporality.ALL_CYCLE ou erreur
	 
	 return(nextTime);
 }
 
 /**
  * Repositionne le TimeSlot issue d'une ex�cution de plusieurs TimeSlot sur une
  * m�me position temporelle.
  * @param ts
  * @param nextTime
  */
 protected void replaceSameSlot(TimeSlot ts,long next_time){
	if(next_time<=endSimulation && ts.ruleCount()>0){
		ts.setTime(next_time);
		nextTimeSlot.place(ts,next_time);
	}
 }
 
 
 //[M�thode priv�
 private void purge(){ //on purge l'ensemble des TimeSlot existant
	 nextTimeSlot=new TimeSlot(this,startSimulation,Temporality.DEFAULT); 
	 forAllCycle=new TimeSlot(this,startSimulation,Temporality.ALL_CYCLE);
	 waitingMessage.clear();
 }
 
 /**
  * Calcul la position temporelle le plus pr�s situ� apr�s 'pos' 
  * pour un cycle qui d�marre a la position 'ref' de periode 'step'
  * @param ref
  * @param pos
  * @param periode
  * @return
  */
 private long nextStep(long ref, long pos, long step){
	 if(pos<=ref) return(ref);
	 float dec=((float) (pos-ref))/((float) step) + 0.5f;
	 return((long) (ref+ (dec * step)));
 }
 
 /**
  * Place le message dans la pile des messages en attente de livraison
  */
 private void add(InternalMessage mess,long dateR){
	 int max=waitingMessage.size(); InternalMessage cur;
	 for(int i=0;i<max; i++){
		 cur=waitingMessage.get(i);
		 if(dateR<=cur.getReceptionDate()) {
			 waitingMessage.add(i,mess);
			 return;
		 }
	 }
	 waitingMessage.add(mess); //on rajoute en fin de liste
 }
 
 /**
  * livre tous les messages dont la date de Reception est <= a la date 'time'
  */
 private void deliverMessages(long time){
	 InternalMessage cur;
	 while(!waitingMessage.isEmpty()) {
		 cur=waitingMessage.poll();
		 if(cur.getReceptionDate()<=time) { //on livre ce message
			 cur.setReceptionDate(time);
			 kernel.deliverMessages(cur);
		 } else {
			 waitingMessage.addFirst(cur);
			 return;	
		 }
	 }
 }
 
 //]
}