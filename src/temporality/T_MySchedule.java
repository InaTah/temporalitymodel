package temporality;

import repast.simphony.engine.schedule.ScheduleParameters;

/**
 * InternalTemporalRule.java
 * */


public class T_MySchedule implements T_IMyTemporalRule{

	String id;		//identifiant
	InternalTemporality temporality;
	T_MyScheduleParameters scheduleParams;
	double timeCanceled,timeActivated;

	int compteur;
	T_MyTimeSlot timeSlot;	//prochain timeSlot d'exécution (ou time slot courant si la rêgle est activé)
	double nextTime;	//prochaine d'ate d'exécution (-1 si indéterminé), NB: si la rêgle n'est pas activé on a nextTime==timeSlot.getTime()

	public T_MySchedule(InternalTemporality tempo,String ruleId, T_MyScheduleParameters scheduleParams) {
		this.timeActivated=-2;
		this.timeCanceled=-2;
		this.temporality=tempo;
		this.id=ruleId;
		define(scheduleParams);
	}

	public void define(T_MyScheduleParameters scheduleParams){
		this.scheduleParams = scheduleParams;
		compteur=0;
		timeCanceled=-1;
		nextTime=-1;
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return id;
	}


	@Override
	public double getBeginTime() {
		// TODO Auto-generated method stub
		return scheduleParams.getStart();
	}


	@Override
	public int cycleCount() {
		// TODO Auto-generated method stub
		return compteur;
	}

	@Override
	public double getNextTime() {
		// TODO Auto-generated method stub
		return nextTime;
	}

	@Override
	public void cancelNext(boolean flag) {// si flag==true annul la prochaine activation de cette règle (cette opération n'est possible que si cette régle n'est ou n'a pas déja été activé au cours du cycle courant et que la prochaine date d'activation est connu)
		// TODO Auto-generated method stub 
		if(flag) { if(!isActivated()) timeCanceled=timeSlot.getTime(); }
		else timeCanceled=-2;

	}
//=============================================================== Todo=========================================
	@Override
	public boolean isCanceled() {
		// TODO Auto-generated method stub
		return timeCanceled==timeSlot.getTime();
	}

	@Override
	public boolean isActivated() {
		// TODO Auto-generated method stub
		return timeActivated==temporality.getTime();
	}
	
	public long getSlotTime(){	//lecture de la date du TimeSlot affect� a cette r�gle
		 if(timeSlot!=null) return(timeSlot.getTime());
		 return(-1);
	 }
	
	public void setTimeSlot(T_MyTimeSlot ts){
		 timeSlot=ts;
		 nextTime=-1;	//indéterminé
		 if(scheduleParams.getPeriod()==Temporality.PONCTUAL) scheduleParams.setEnd(ts.getTime()); //rêgle ponctuelle
	 }
	 
	 public T_MyTimeSlot getTimeSlot() { return(timeSlot); }
	 
	 public void setNextTime(long nt) { nextTime=nt; }
	 
	 public boolean isCanceled(long t){ return(timeCanceled==t);}
	 
	 /**
	  * Active cette rêgle temporelle (=> execute le comporte associé)
	  */
	 public void run(){
		 compteur++;
		 timeActivated=temporality.getTime();
		 temporality.run(this);
	 }


}
