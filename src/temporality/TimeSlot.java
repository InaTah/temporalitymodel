package temporality;
import java.util.*;

/*
 * Représente un timecode
 */
public class TimeSlot {

 //[Attribut
 Timer timer;	
 long time;		//date du slot temporel
 long step;		//pas d'incrémentation des règles inscrite dans ce TimeSlot
 //]
 
 //[Gestion de la liste chainée des TimeSlot (L'ensemble des TimeSlot instancié forme une liste chainée)
 TimeSlot sameSlot;	//autre TimeSlot a exécuter dans le même slot temporel
 TimeSlot nextSlot;	//prochain slot temporel a être exécuté (le prochain dans la liste chainé)
 //]
 
 //[Liste des règles temproelles addossées à ce TimeSlot
 Vector<InternalTemporalRule> trList; //liste des rêgles inscrite dans ce TimeSlot 
 //]	
 
 //Méthode
 public TimeSlot(Timer v_timer,long v_time,long periode){	 
	 timer=v_timer;
	 time=v_time;
	 step=periode;
	 sameSlot=null;
	 nextSlot=null;
	 trList=new Vector<InternalTemporalRule>();
 }
 
 public long getTime(){ return(time); }
 public void setTime(long t){ 
	 time=t;
	 nextSlot=null;
	 sameSlot=null;
 }
 public long getPeriod() { return(step); } 
 public int ruleCount(){	//nombre de r�gle temporelle rattach� a ce slot
	 return(trList.size());
 }
 public TimeSlot getNextSlot(){ return(nextSlot); }
 /**
  * Ajoute la r�gle dans ce TimeSlot
  */
 public void add(InternalTemporalRule tr){
//System.out.println("mis a "+time);	 
	 tr.setTimeSlot(this);
	 trList.add(tr);
 }
 
 /**
  * Retire la r�gle de ce TimeSlot
  */
 public void remove(InternalTemporalRule tr){
	 tr.setTimeSlot(null);
	 trList.remove(tr);
 }
 
 protected void setNextSlot(TimeSlot ts) { //Attention on considere que ts.nextSlot==NULL !
	 if(ts!=null) ts.nextSlot=nextSlot;
	 nextSlot=ts;
 }

 
 /**
  * traite le placement d'une régle (TemporalRule) dans le TimeSlot approprié
  */
 public void place(InternalTemporalRule tr,long atTime,long periode){
	 if(atTime>(time+timer.getMinimalStep())){ //celle régle doit être placé dans les futurs slots temporels
		 if(nextSlot==null) nextSlot=new TimeSlot(timer,timer.nextSlot(atTime),periode);  //on crée un nouveau slot pour la période de tr
		 else { //on regarde si on doit créer un slot intermédiaire entre celui-ci est le prochain
			 long dif=nextSlot.getTime()-atTime;
			 if(dif>0 && dif>=timer.getMinimalStep()){ //oui !
				 TimeSlot newSlot=new TimeSlot(timer,timer.nextSlot(atTime),periode);  //on crée un nouveau slot pour la période de tr
				 setNextSlot(newSlot);
			 }
		 }
		 nextSlot.place(tr,atTime,periode);
	 } else if(periode==Temporality.PONCTUAL || periode==step) add(tr);
	 else {
		 if(sameSlot==null) sameSlot=new TimeSlot(timer,time,periode);  //on crée un nouveau slot pour la période de tr
		 sameSlot.place(tr,atTime,periode);
	 }
 }
 
 /**
  * Traite le placement d'un TimeSlot sur la chaine temporelle
  */
 public void place(TimeSlot t,long atTime){
	 if(time==atTime){ //on va le placer sur le même slot temporelle
		 if(t.getPeriod()==step) t.replaceAllInSlot(this); //même périodicité, on fusionne les listes de règle temporelle
		 else {
			 if(sameSlot!=null) sameSlot.place(t,atTime); //on parcours en récursif pour traiter une éventuelle fusion
			 else sameSlot=t;
		 }
	 } else {
		 if(nextSlot==null) nextSlot=t;
		 else if(atTime<nextSlot.getTime()){ //on insére t entre ici et le prochain slot
			 t.setNextSlot(nextSlot);
			 nextSlot=t;
		 } else nextSlot.place(t,atTime);
	 }
 }
 
 public void trace(){
	 System.out.println("time="+time+" step="+step);
	 if(sameSlot!=null) sameSlot.traceSame();
	 if(nextSlot!=null) nextSlot.trace();
 }
 public void traceSame(){
	 System.out.println(" -same- time="+time+" step="+step);
	 if(sameSlot!=null) sameSlot.traceSame();
 }
 
 
 /**
  * Execute tous les temporalit� a d�clencher sur ce TimeSlot, et sur ceux
  * situ� sur le m�me slot temporelle en traitant leur replacement sur le prochain
  * slot temporelle appropri�
  */
 public long run(){
	 long nextTime=timer.nextStepFor(this); //on calcul le prochain temps d'ex�cution pour ce TimeSlot
	 
	 InternalTemporalRule cur;
	 for(int i=0; i<trList.size(); i++){
		 cur=trList.get(i);
		 if(cur.getEndTime()<time) {
			 timer.unregister(cur); //cette r�gle n'est plus valide
			 i--;
		 } else {
			 cur.setNextTime(nextTime);
			 if(!cur.isCanceled(time)) cur.run();
		 }
	 }
	 
	 if(sameSlot!=null){
		 long t=sameSlot.run();	//on execute tous les TimeSlot situ� sur le m�me slot
		 timer.replaceSameSlot(sameSlot,t);
	 }

	 return(nextTime); 	 //on retourne la date de prochaine ex�cution de ce TimeSlot
 }

 /**
  * Execute tous les temporalit� a d�clencher sur ce TimeSlot, comme �tant une 
  * ex�cution a tous les cycles.
  */
 public void runAsAllCycle(){	
	 InternalTemporalRule cur;
	 for(int i=0; i<trList.size(); i++){
		 cur=trList.get(i);
		 if(cur.getEndTime()<time){
			 timer.unregister(cur);
			 i--;
		 } else if(cur.getBeginTime()>=time && !cur.isCanceled(time)) cur.run();
	 }
 }
 
 /**
  * r�affecte l'ensemble des r�gle port� sur ce slot
  */
 protected void replaceAllInSlot(TimeSlot ts){
	 for(InternalTemporalRule cur: trList) ts.add(cur);
 }
}