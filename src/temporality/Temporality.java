package temporality;

/**
 * Structure de contrôle du comportement temporel de l'agent.
 * <br><br>
 * <b>Voir :</b> <a href="../../../tutorial/agent.html">Programmer un agent GNG</a>
 */
public interface Temporality {

 //[Définition des constantes :
	//Constante pour la valeur beginTime :
 /**
  * Constante de date qui désigne la date actuelle
  */
 public final static long NOW=-1;		//débute dés que possible
// public final static long NEXT_STEP=-3;	//date actuel + une periode
	
 	//Constante pour la valeur endTime :
 /**
  * Constante de date qui désigne la date de fin de simulation
  */
 public final static long AT_END=-1;	//jusqu'a la fin de la simulation 
 
 	//Constante pour la valeur Gap :
 /**
  * Constante de durée qui désigne une durée libre (non contrainte)
  */
 public final static long FREE=-1;		//écart libre
 /**
  * Constante de durée qui désigne la durée la plus petite possible
  */
 public final static long MINIMAL=-2;	//écart le + petit possible

 	//Constante pour les période
 /**
  * Constante de période qui désigne une ponctualité (évènement non périodique)
  */
 public final static long PONCTUAL=-1;	//non périodique (a déclencher qu'une fois)
 
 /**
  * Constante de période qui désigne la période par défaut spécifié dans le paramétrage de la simulation
  */
 public final static long DEFAULT=-4;	//période par défaut
 //public final static long NEXT_CYCLE=-2; //a déclencher seulement au prochain cycle
 
 /**
  * Constante de période qui désigne une périodicité correspondant a tous les cycles d'exécution qui seront déclenchés
  */
 public final static long ALL_CYCLE=-3;	//a déclencher a tous les cycles
 //]
 
 /**
  * Retourne la date simulée courante (-1 si la simulation n'a pas encore été lancée)
  */
 public long getTime();  //consultation de la date courante 
 
 //[information sur la règle temporelle qui a déclenché l'exécution courante
 
 /**
  * Fournit la règle temporelle qui à déclenché l'activation actuelle (null si la simulation n'a pas encore été lancé)
  */
 public TemporalRule getRule();	//règle temporal qui a déclenché cette exécution (règle active)
 
 /**
  * Indique l'identifiant de la règle temporelle qui à déclenché l'activation actuelle (null si la simulation n'a pas encore été lancé)
  */
 public String getRuleID();		//identifiant de la règle de temporalité actuel
 //]

 //[cas ou plusieurs temporalité sont activable dans le cycle courant
 /**
  * Retourne le nombre de règle temporelle potentiellement encore potentiellement ativable dans le cycle courant
  * (même si la règle est annulée, elle reste potentiellement activable car son annulation peut être levé dans le code)
  */
 public int temporalityCount();			//nombre de règle temporelle encore théoriquement activable (même celle annulée) pour ce cycle
 /**
  * Retourne la liste des identifiant des règle temporelle potentiellement encore potentiellement ativable dans le cycle courant
  * (même si la règle est annulée, elle reste potentiellement activable car son annulation peut être levé dans le code)
  */
 public Iterable<String> allActivableRuleID();	//retourne l'ensemble des identifiant des rêgles qui sont encore théoriquement activable (même celle annulée) pour ce cycle 
 //]
 
 //[Accés aux règles de temporalité défini
 /**
  * Retourne l'ensemble des identifiants de rêgle déja défini
  */
 public Iterable<String> allRuleID();
 /**
  * Lecture d'une règle temporelle
  * @param ruleId identifiant de la règle
  * @return l'intance TemporalRule de la règle demandé
  */
 public TemporalRule getRule(String ruleId);
 /**
  * Creation ou redéfinition d'une règle temporelle.
  * <br>
  * Cette règle sera valable dans l'interval de temps allant de la date courante à la fin de la simulation.
  * @param ruleId identifiant à attribuer à la règle
  * @param periode période d'activation de la règle
  * @return l'intance TemporalRule de la règle créée ou modifiée (si elle existait déjà) 
  */
 public TemporalRule newRule(String ruleId,long periode); //défini ou remplace une règle temporelle
 /**
  * Creation ou redéfinition d'une règle temporelle.
  * @param ruleId identifiant à attribuer à la règle
  * @param beginTime date de début de validité de la règle
  * @param endTime date de fin de validité de la règle
  * @param periode période d'activation de la règle
  * @param gap tolérance d'écart de date pour cette règle
  * @return l'intance TemporalRule de la règle créée ou modifiée (si elle existait déjà)
  */
 public TemporalRule newRule(String ruleId,long beginTime,long endTime,long periode,long gap); //défini ou remplace une règle temporelle (periode==-1 si régle ponctuel)
 
 /**
  * Suppression d'une règle
  * @param ruleId identifiant de la règle à supprimer
  * @return true si la règle a été supprimer, ou false si l'identifiant indiqué ne correspond a aucune règle existante
  */
 public boolean removeRule(String ruleId);
 //]
 
 //[informations générales
 public long timeStartSimulation();
 public long timeEndSimulation();
 //]
}