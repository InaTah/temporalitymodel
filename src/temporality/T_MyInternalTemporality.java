package temporality;

import java.util.Collection;
import java.util.Hashtable;
import java.util.Vector;

import agent.AgentDescriptor;

public class T_MyInternalTemporality implements T_MyTemporality{
	//Attribut
	//AgentDescriptor descriptor;
	InternalTemporalRule activeRule;	//règle active
	Hashtable<String,InternalTemporalRule> rules;
	Collection<InternalTemporalRule> collectionRules;
	Timer timer;
	Vector<String> activableRules;	//liste des règle encore activable au cours de ce cycle
	double activableRulesTimeRef; //date de référence du vecteur activableRule

	//Méthode
	public T_MyInternalTemporality( Timer t){
		timer=t;
		activeRule=null;
		rules=new Hashtable<String,InternalTemporalRule>();
		collectionRules=null;
		activableRules=new Vector<String>(); activableRulesTimeRef=-2;
	}

	//[Interface Temporality
	public double getTime(){  //consultation de la date courante
		if(activeRule!=null) return(activeRule.getSlotTime());
		else return(timer.timeStartSimulation());
	}

	//[information sur la régle temporelle qui a déclenché l'exécution courante
	public T_IMyTemporalRule getRule(){	//règle temporelle qui a déclenché cette exécution
		return(activeRule);
	}
	public String getRuleID(){		//identifiant de la régle de temporalité actuel
		if(activeRule!=null) return(activeRule.getId());
		else return(null);
	}
	//]

	//[cas ou plusieurs temporalité vont être mis en oeuvre dans ce même cycle
	public int temporalityCount(){			//nombre de rêgle temporelle activable (c.a.d. qui vont encore être activ� au cours de ce cycle)
		if(activableRulesTimeRef!=getTime()) majActivableRules();
		return(activableRules.size());
	}
	public Iterable<String> allActivableRuleID(){	//retourne l'ensemble des identifiant des rêgles qui sont encore théoriquement activable (même celle annulée) pour ce cycle
		if(activableRulesTimeRef!=getTime()) majActivableRules();
		return(activableRules);
	}
	private void majActivableRules(){	//rêgle temporelle activable numéro n
		activableRules.clear();
		double time=getTime();
		for(InternalTemporalRule cur: rules.values()) 
			if(cur.getNextTime()==time) activableRules.add(cur.getId());
	}
	//]

	//[Accés aux rêgles de temporalité défini
	public Iterable<String> allRuleID(){					  //retourne l'ensemble des identifiant de rêgle déja défini
		return(rules.keySet());
	} 
	public TemporalRule getRule(String ruleId){
		//		 if(ruleId==null) return(null);
		return(rules.get(ruleId));
	}
	public TemporalRule newRule(String ruleId,double periode){ //d�fini ou remplace une r�gle temporelle
		return(newRule(ruleId,Temporality.NOW,Temporality.AT_END,periode,Temporality.FREE));
	}
	public TemporalRule newRule(String ruleId,double beginTime,double endTime,double periode,double gap){ //d�fini ou remplace une r�gle temporelle (periode==-1 si r�gle ponctuel)
		//		 if(ruleId==null) return(null);
		InternalTemporalRule rule=rules.get(ruleId);

		if(beginTime==Temporality.NOW) beginTime=getTime();
		if(endTime==Temporality.AT_END) endTime=timer.timeEndSimulation();


		if(rule==null) rule=new InternalTemporalRule(this,ruleId,beginTime,endTime,periode,gap);
		else rule.define(beginTime,endTime,periode,gap);

		descriptor.register(rule);	//enregistrement de la régle auprés du gestionnaire de slot d'exécution
		rules.put(ruleId,rule);
		return(rule);	 
	}
	public boolean removeRule(String ruleId){
		InternalTemporalRule rule=rules.get(ruleId);
		if(rule==null) return(false);

		rules.remove(ruleId);
		descriptor.unregister(rule);
		activableRules.remove(ruleId);

		return(false);	 
	}

	//]
	//[informations générales
	public double timeStartSimulation(){ return(timer.timeStartSimulation());}
	public double timeEndSimulation(){ return(timer.timeEndSimulation());}
	//]

	//]

	//[M�thode prot�g�es
	/**
	 * ré-initialisation de l'instance
	 */
	public void reset(){
		activeRule=null;
		rules.clear();
		activableRulesTimeRef=-2;
	}

	/**
	 * Consultatuon du nombre de r�gle d�fini
	 */
	public int countRule(){
		return(rules.size());
	}

	/**
	 * Ex�cution du comportement de l'agent d�clench� par la regle rule
	 */
	public void run(InternalTemporalRule rule){
		activeRule=rule;
		if(activableRulesTimeRef==getTime()) activableRules.remove(rule.getId());
		descriptor.run();
		activeRule=null;
	}
	//]
}
