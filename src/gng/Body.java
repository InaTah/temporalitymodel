package gng;
/**
 * Corps (matérialisé ou non) des agents dans l'environnement.
 * <br>
 * Une instance de ce type est fourni aux agents en argument de leur méthode run,
 * et c'est celle-ci qui leur permet intéragir avec l'environnement.
 * <br>
 * Le choix du plan d'environnement dans lequel les interactions doivent avoir lieu est
 * déterminé par la face sur laquelle est positionné l'instance Body.
 * <br><br>
 * <b>Voir :</b> <a href="../../../tutorial/agent.html">Programmer un agent GNG</a> 
 */
public interface Body /*extends MultiFace<Body>*/ {

 /**
  * Exécution d'une requête de perception.
  * @param requete requete de perception
  * @return résultat de la perception renvoyé par l'environnement.
  */	
 public ReadableMultiChamp perceive(ReadableMultiChamp requete);
 
 /**
  * Exécution d'une requête d'influence.
  * @param requete requete d'influence
  * @return l'identifiant de l'influence, ou null si l'influence n'est pas supporté par l'environnement.
  */
 public String influence(ReadableMultiChamp requete);
 
 /**
  * Consultation de la réaction produite par une influence exercé aux cycle précédent.
  * @param idInfluence identifiant de l'influence a consulter
  * @return résultat de l'influence, ou null si l'influence est un échec
  */
 public ReadableMultiChamp reaction(String idInfluence);
 
}