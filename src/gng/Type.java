package gng;

/**
 * Définition des contantes désignant les types supportés.
 * <br><br>
 * <b>Voir :</b> <a href="../../../tutorial/type/index.html">Structure de donnée de GNG</a>
 */
public interface Type {
	
	/**
	 * Traduit la non existance de la valeur.
	 */
	public final static byte NO_EXIST=-1;  
	
	/**
	 * Valeur non affecté (son type n'est alors pas déterminable).
	 */
	public final static byte NULL=0;
	
	/**
	 * Constante pour désigner le type boolean
	 */
	public final static byte BOOLEAN=1;  

	/**
	 * Constante pour désigner le type char
	 */	
	public final static byte CHAR=2;
	
	/**
	 * Constante pour désigner le type short
	 */
	public final static byte SHORT=3;
	
	/**
	 * Constante pour désigner le type int
	 */
	public final static byte INT=4;
	
	/**
	 * Constante pour désigner le type long
	 */
	public final static byte LONG=5;  
	
	/**
	 * Constante pour désigner le type float
	 */
	public final static byte FLOAT=6;
	
	/**
	 * Constante pour désigner le type double
	 */
	public final static byte DOUBLE=7;
	
	/**
	 * Constante pour désigner le type String
	 */
	public final static byte STRING=8;
	
	/**
	 * Constante pour désigner le type byte[]
	 */
	public final static byte BYTES=9;		//tableau de byte  ( byte[] )
	
	/**
	 * Constante pour désigner le type <a href="ReadableMultiChamp.html">ReadableMultiChamp</a>
	 */
	public final static byte MCHAMP=10;		//Multi-Champ
	  
}