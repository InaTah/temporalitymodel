package gng;

public interface Message {
	
	//Constante des signalement d'erreurs par le receveur du message
	public final static byte UNKNOW=1;		//message incompr�hensible
	public final static byte UNEXPECTED=2;	//message inattendu
	public final static byte MISSING=3;		//il manque un argument dans le message
	
	public String getExpeditor();	
	public String getDestinataire();

	public long getExpeditionDate();	//date d'expédition
	public long getReceptionDate();		//date de réception

	public long getDelay();	//consultation du delay de livraison spécifié pour ce message
	
	public Object getContent();					//contenu du message

	public ReadableMultiChamp getMCContent();	//contenu du message de type ReadableMultiChamp (null si le type n'est pas compatible) 
	public String getStringContent();			//contenu.toString() du message

	/**
	 * Le receveur signal une erreur pour le traitement de ce message
	 */
	public void addError(byte type,String detail);
}