package gng;

public interface Kernel {

  //[Gestion des agents : pour les sous-modules EModel
//pkk  public String register(String family,Agent a,String id,ReadableMultiChamp bodyParam); //Inscrit un nouvelle agent dans le noyau
  public String register(String family,String id,ReadableMultiChamp classParam,ReadableMultiChamp bodyParam); //Inscrit un nouvelle agent dans le noyau
  public boolean unregister(String id);
//  public State getAgentState(String id);
  //]
  
  //[Gestion de la simulation : généralement invoqué par le composant GUI de pilotage de la simualtion
  public boolean startSimulation(Environment env,long start,long end,long step,long mini);
  public boolean stopSimulation();
  public boolean suspendSimulation();
  public boolean resumeSimulation();
  //]

  //[Gestiond des observations de la simualtion
  public void addSimulationObserver(SimulationObserver o);
  public boolean removeSimulationObserver(SimulationObserver o);  
  //]
/* 
  //[Gestion des observations de l'état des agents
  public boolean addStateFamilyListener(StateListener listener, String family, String attribut);
  public boolean addStateAgentListener(StateListener listener, String idAgent, String attribut);
  public boolean removeStateAgentListener(StateListener listener, String idAgent, String attribut);
  public boolean removeStateFamilyListener(StateListener listener, String family, String attribut);
  //]
*/  
  public void fireRunCycle(long time,long startSimulation,long endSimulation);
  public void fireEndSimulation();
  public void deliverMessages(InternalMessage mess);
}