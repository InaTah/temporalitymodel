package gng;

public interface SimulationObserver {

 public void notifySimulationStart(long startTime,long endTime);
 public void notifySimulationEnd();
 public void notifyRunCycle(long curTime,long startTime,long endTime);		//signale le d�but d'�x�cution du cycle de timecode 'time'

}