package gng;

/**
 * Interface d'une structure de transtypage accessible en lecture seulement.
 * <br><br>
 * <b>Voir :</b> <a href="../../../tutorial/type/index.html">Structure de donnée de GNG</a>
 */
public interface ReadablePropertie extends Type {
	
	public byte getType();
	public boolean toBoolean();
	public char toChar();
	public short toShort();
	public int toInt();
	public long toLong();
	public float toFloat();
	public double toDouble();
	public String toString();
	public byte[] toBytes();
	public ReadableMultiChamp toMultiChamp();
	
}