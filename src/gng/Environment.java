package gng;

import java.awt.*;

public interface Environment /*extends MultiFace<Environment> */{

 //[Pour le Kernel
// public Body newBody(ReadableMultiChamp param);
 public void run(long currentTime);
 //]
 
 //[Enregistrement des plans d'information
// public boolean registerPlan(Plan plan); 
// public boolean registerMapPlan(String name_plan,String label,BufferedImage i,float scale_i_to_r,float scale_p_to_r, ColorOperator op);
 public boolean registerDrawerPlan(String name_plan,String label,Dimension dimension);
 //]
 
 //[Opération sur le plan courant
 public ReadableMultiChamp configRequest(ReadableMultiChamp requete); //execute une requete de configuation sur le plan d'information 
 public ReadablePropertie read(int x, int y, int width, int height);
// public ColorLegend getLegend();
 public String getLabel(); 
 //]
  
 //[Manipulation des clients d'affichage
// public boolean addDisplayClient(DisplayClient client);
// public boolean removeDisplayClient(DisplayClient client);
 
 /**
  * Dessine l'intégralité de la carte dans le rectangle indiqué
  */ 
 public void drawMap(Graphics g, Rectangle destination, float scaleRealToPixel, Component component, byte drawableMode);
 public void drawMap(Iterable<String> names_plan,Graphics g, Rectangle destination, float scaleRealToPixel, Component component, byte drawableMode);
 
 /**
  * Dessine la portion de carte demandé
  * bounds!=null , sera rempli avec les dimensions réel de l'étendue 
  * de la zone concerné.
  */
 public void drawZone(Graphics g, int sx_real, int sy_real, Rectangle destination, float scaleRealToPixel, Component component, byte drawableMode, Rectangle bounds);
 public void drawZone(Iterable<String> names_plan,Graphics g, int sx_real, int sy_real, Rectangle destination, float scaleRealToPixel, Component component, byte drawableMode, Rectangle bounds);
 //]
 /*
 //[Manipulation des éléments a afficher
 public boolean addDrawable(Drawable client);
 public boolean removeDrawable(Drawable client);
 public void refresh(Drawable d); 			//met a jour l'affichage suite a la modification du drawable d
 public void refresh(Rectangle zone); 		//met a jour la zone d'affichage indiqu�
 */
 public void clickAt(boolean b_left,int x,int y);
 //] 
 
 //[Manipulation des échelles
 public Dimension getDimension();	//dimension du plan d'information 
 public float getScaleImageToReal();
 public float getScalePixelToReal(); 
 public float convertItoR_to_RtoP(float v_scaleImageToReal); 
 public float convertPtoR_to_ItoR(float v_scalePixelToReal);
 //]
 /*
 //[Gestion des observations de l'état des objets d'environnement
 public boolean addFamilyListener(EnvironmentListener listener, String family, String attribut);
 public boolean addObjectListener(EnvironmentListener listener, String idObject, String attribut);
 public boolean removeObjectListener(EnvironmentListener listener, String idObject, String attribut);
 public boolean removeFamilyListener(EnvironmentListener listener, String family, String attribut);
 //]
  * */
 
}