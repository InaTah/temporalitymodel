package gng;

public interface State /*extends WritableMultiChamp */{

 public String getId();		//identifiant de l'entité représenté par cette structure d'état
 public String getFamily();	//nom de la famille de l'entité représenté par cette structure d'état
 
}