package gng;

/**
 * Interface d'une structure multi-champ accessible en lecture seulement.
 * <br><br>
 * <b>Voir :</b> <a href="../../../tutorial/type/index.html">Structure de donnée de GNG</a>
 */
public interface ReadableMultiChamp extends Type {
 
 //[Introspection de la structure
 public Iterable<String> getFieldNames(); 
 public boolean exist(String name);		//test l'existance d'un champ
 //]
 
 //[Lecture de la valeur d'un champ:  public [T] get[T](String name);
 public byte getType(String name);
 public boolean getBoolean(String name);
 public char getChar(String name);
 public short getShort(String name);
 public int getInt(String name);
 public float getFloat(String name);
 public long getLong(String name);
 public double getDouble(String name);
 public String getString(String name);
 public byte[] getBytes(String name);
 public ReadableMultiChamp getMultiChamp(String name);
 public ReadablePropertie get(String name);
 
 	//avec spécification d'une valeur par défaut en cas ou la propriété n'existe pas  : public [T] get[T](String name,[T] ifnoexist);
 public boolean getBoolean(String name, boolean ifnoexist);
 public char getChar(String name, char ifnoexist);
 public short getShort(String name, short ifnoexist);
 public int getInt(String name, int ifnoexist);
 public float getFloat(String name, float ifnoexist);
 public long getLong(String name, long ifnoexist);
 public double getDouble(String name, double ifnoexist);
 public String getString(String name, String ifnoexist);
 public byte[] getBytes(String name, byte[] ifnoexist);
 public ReadableMultiChamp getMultiChamp(String name, ReadableMultiChamp ifnoexist); 
 public ReadablePropertie get(String name, ReadablePropertie ifnoexist);
 //]
 
}