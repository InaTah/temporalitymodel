package gng;

public interface Mailbox {
	
  public int mailCount();	//nombre de message dans la boite
  
  public Message get(int pos);	//accés au message numéro 'pos' (sans l'enlever de la liste)		

  public Message remove(int pos);	//supprime le message situé à la position 'pos'		  
  
  public Message poll();	//dépile le prochain message, null si vide
  
  public void send(String dest,Object content,long delay);	//envoie un message
 
}