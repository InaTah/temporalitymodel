package gng;

public class InternalMessage implements Message {
	
 //Attribut
 String expeditor,destinataire;
 Object content;	
 long delay,dateExp,dateRecept;
 
 public InternalMessage(String exp,String dest,Object c,long dateE, long d, long dateR){
	 expeditor=exp;
	 destinataire=dest;
	 content=c;
	 dateExp=dateE;
	 delay=d;
	 dateRecept=dateR;
 }
 
 //[Interface Message
 public String getExpeditor() { return(expeditor); }	
 public String getDestinataire() { return(destinataire); }
	
 public Object getContent() { return(content); }//contenu du message
 
 public ReadableMultiChamp getMCContent(){	//contenu du message de type ReadableMultiChamp (null si le type n'est pas compatible)
	 if(content instanceof ReadableMultiChamp) return((ReadableMultiChamp) content);
	 else return(null);
 }
 
 public String getStringContent() { 			//contenu.toString() du message
	 if(content!=null) return(content.toString());
	 else return(null);
 }

 public long getExpeditionDate(){ return(dateExp); }	//date d'exp�dition
 public long getReceptionDate() { return(dateRecept); }	//date de r�ception
	
 public long getDelay() { return(delay); }	//consultation du delay de livraison spécifié pour ce message
 
	/**
	 * Le receveur signal une erreur pour le traitement de ce message
	 */
 public void addError(byte type,String detail){
System.out.println("[ERROR MESSAGE] exp: "+expeditor+" - dest: "+destinataire+" - detail: "+detail); //OXO	 
 }
 //]
 
 public void setReceptionDate(long v) { dateRecept=v; }
}